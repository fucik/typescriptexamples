function addVAT(price, vat = 0.2) {
    return price * (1 + vat);
}
const vatPrice = addVAT(30, 0.2); // OK! 
const vatPriceWithDefault = addVAT(30); // OK!
// deliveryAddress is of type any
let deliveryAddress = '421 Smashing Hill, 90210';
let deliveryAddress2 = '421 Smashing Hill, 90210';
// array
let deliveryAddresses = [];
deliveryAddresses.push('421 Smashing Hill, 90210');
function selectDeliveryAddress(addressOrIndex) {
    if (typeof addressOrIndex === 'number') {
        return deliveryAddresses[addressOrIndex];
    }
    return addressOrIndex;
}
// any bych se měl snažit vyhnout
// unknown
function selectDeliveryAddresses(addressOrIndex) {
    if (typeof addressOrIndex === 'number' && addressOrIndex < deliveryAddresses.length) {
        return deliveryAddresses[addressOrIndex];
    }
    else if (typeof addressOrIndex === 'string') {
        return addressOrIndex;
    }
    return '';
}
const movie = {
    title: 'Helvetica',
    price: 6.66,
    vat: 0.19,
    stock: 1000,
    description: '90 minutes of gushing about Helvetica'
};
// Structural Typing and Excess Property Checks
const movBackup = {
    title: 'Helvetica',
    price: 6.66,
    vat: 0.19,
    stock: 1000,
    description: '90 minutes of gushing about Helvetica',
    rating: 5
};
const movie2 = movBackup; // Totally OK!
// Objects as Parameters
function createArticleElement(article) {
    const title = article.title;
    const price = addVAT(article.price, article.vat);
    return `<h2>Buy ${title} for ${price}</h2>`;
}
const shopItem = {
    title: 'Helvetica', price: 6.66,
    vat: 0.19,
    stock: 1000,
    description: '90 minutes of gushing about Helvetica',
    rating: 5
};
createArticleElement2(shopItem); // Totally OK!
function createArticleElement2(article) {
    const title = article.title;
    const price = addVAT(article.price, article.vat);
    return `<h2>Buy ${title} for ${price}</h2>`;
}
function isArticleInStock(article) {
    // this check is necessary to make sure
    // the optional property exists 
    if (article.stock) {
        return article.stock > 0;
    }
    return false;
}
// import { Article3 } from './example-two'
// class
class Discount {
    constructor(isPercentage, amount) {
        this.isPercentage = isPercentage;
        this.amount = amount;
    }
    apply(article) {
        if (this.isPercentage) {
            article.price = article.price
                - (article.price * this.amount);
        }
        else {
            article.price = article.price - this.amount;
        }
    }
}
let discount = new Discount(true, 0.2);
// Extending Classes
class TwentyPercentDiscount extends Discount {
    constructor() {
        super(true, 0.2);
    }
    apply(article) {
        if (this.isValidForDiscount(article)) {
            super.apply(article);
        }
    }
    isValidForDiscount(article) {
        return article.price <= 40;
    }
}
const discount2 = new Discount(true, 0.2);
const shopItem2 = {
    title: 'Inclusive components',
    price: 30,
    vat: 0.2
};
// Discount.apply is typed to take `Article` 
// but also takes a `ShopItem` 
discount.apply(shopItem);
// Implementing Interfaces
class DVD {
    constructor(title) {
        this.title = title;
        this.price = 9.99;
        this.vat = 0.2;
    }
}
// Implementing Types
class Book {
    constructor(title) {
        this.title = title;
        this.price = 39;
        this.vat = 0.2;
    }
}
// Property Access Modifiers
class Article4 {
    constructor(title, price) {
        this.title = title;
        this.price = price;
    }
}
const article = new Article4('Smashing Book 6', 39);
//console.log(article.price) nejde
// Abstract Classes
class Discount2 {
    // This is already implemented
    apply(article) {
    }
}
// Enums
var UserType;
(function (UserType) {
    UserType[UserType["Admin"] = 0] = "Admin";
    UserType[UserType["PayingCustomer"] = 1] = "PayingCustomer";
    UserType[UserType["Trial"] = 2] = "Trial";
})(UserType || (UserType = {}));
function showWarning(user) {
    switch (user) {
        case UserType.Admin: return false;
        case UserType.PayingCustomer: return false;
        case UserType.Trial: return false;
    }
}
search('Ember'); // Yes!
search('Ember', ['JavaScript']); // Also yes! 
search('Ember', ['JavaScript', 'CSS']); // Yes yes!
// Asynchronous Back-End Calls
function search2(query, tags) {
    let queryString = `?query=${query}`;
    // tags can be undefined as it's optional. // let's check if they exist
    if (tags && tags.length) {
        // and add all tags in that array to the // query string
        queryString += `&tags=${tags.join()}`;
    }
    // Ready? Fetch from our search API
    return fetch(`/search${queryString}`)
        .then(response => response.json());
}
const query = {
    query: 'Ember',
    tags: ['javascript'], assemble(includeTags = false) {
        let query = `?query=${this.query}`;
        if (includeTags && typeof this.tags !== 'undefined') {
            query += `&${this.tags.join(',')}`;
        }
        return query;
    }
};
export {};
