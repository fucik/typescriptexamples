function addVAT(price: number, vat: number = 0.2): number {
    return price * (1 + vat)
}

const vatPrice = addVAT(30, 0.2) // OK! 
const vatPriceWithDefault = addVAT(30) // OK!

// deliveryAddress is of type any
let deliveryAddress: any = '421 Smashing Hill, 90210'

let deliveryAddress2: string = '421 Smashing Hill, 90210'

// array
let deliveryAddresses: string[] = []
deliveryAddresses.push('421 Smashing Hill, 90210')


function selectDeliveryAddress(addressOrIndex: any) {
    if (typeof addressOrIndex === 'number') {
        return deliveryAddresses[addressOrIndex]
    }
    return addressOrIndex
}

// any bych se měl snažit vyhnout


// unknown
function selectDeliveryAddresses(addressOrIndex: unknown): string {
    if (typeof addressOrIndex === 'number' && addressOrIndex < deliveryAddresses.length) {
        return deliveryAddresses[addressOrIndex]
    } else if (typeof addressOrIndex === 'string') {
        return addressOrIndex
    }
    return ''
}

// Composite Types
type Article = {
    title: string, price: number, vat: number, stock: number, description: string
}

const movie: Article = {
    title: 'Helvetica',
    price: 6.66,
    vat: 0.19,
    stock: 1000,
    description: '90 minutes of gushing about Helvetica'
}

// Structural Typing and Excess Property Checks
const movBackup = {
    title: 'Helvetica',
    price: 6.66,
    vat: 0.19,
    stock: 1000,
    description: '90 minutes of gushing about Helvetica',
    rating: 5
}
const movie2: Article = movBackup // Totally OK!

// Objects as Parameters
function createArticleElement(article: Article): string {
    const title = article.title
    const price = addVAT(article.price, article.vat)
    return `<h2>Buy ${title} for ${price}</h2>`;
}


const shopItem = {
    title: 'Helvetica', price: 6.66,
    vat: 0.19,
    stock: 1000,
    description: '90 minutes of gushing about Helvetica',
    rating: 5
}
createArticleElement2(shopItem) // Totally OK!

function createArticleElement2(
    article: {
        title: string, price: number, vat: number
    }): string {
    const title = article.title
    const price = addVAT(article.price, article.vat);
    return `<h2>Buy ${title} for ${price}</h2>`;
}

// Optional Properties
type Article2 = {
    title: string,
    price: number,
    vat: number,
    stock?: number,
    description?: string
}
function isArticleInStock(article: Article) {
    // this check is necessary to make sure
    // the optional property exists 
    if (article.stock) {
        return article.stock > 0
    }
    return false;
}

// Exporting and Importing Types
export type Article3 = {
    title: string, price: number,
    vat: number,
    stock?: number,
    description?: string
}

// import { Article3 } from './example-two'

// class
class Discount {
    isPercentage: boolean
    amount: number
    constructor(
        isPercentage: boolean,
        amount: number) {
        this.isPercentage = isPercentage
        this.amount = amount
    }
    apply(article: Article) {
        if (this.isPercentage) {
            article.price = article.price
                - (article.price * this.amount)
        } else {
            article.price = article.price - this.amount
        }
    }
}


let discount: Discount
    = new Discount(true, 0.2)

// Extending Classes
class TwentyPercentDiscount extends Discount {
    constructor() {
        super(true, 0.2)
    }
    apply(article: Article) {
        if (this.isValidForDiscount(article)) {
            super.apply(article)
        }
    }
    isValidForDiscount(article: Article) {
        return article.price <= 40
    }
}

// Describing Interfaces
// Our friend’s ShopItem
interface ShopItem {
    title: string; price: number;
    vat: number;
    stock?: number; description?: string;
} // And yes, the semicolons are optional

const discount2 = new Discount(true, 0.2)
const shopItem2: ShopItem = {
    title: 'Inclusive components',
    price: 30,
    vat: 0.2
}

// Discount.apply is typed to take `Article` 
// but also takes a `ShopItem` 
discount.apply(shopItem)

// Implementing Interfaces
class DVD implements ShopItem {
    title: string
    price: number
    vat: number
    constructor(title: string) {
        this.title = title
        this.price = 9.99
        this.vat = 0.2
    }
}
// Implementing Types
class Book implements Article2 {
    title: string
    price: number
    vat: number
    constructor(title: string) {
        this.title = title
        this.price = 39
        this.vat = 0.2
    }
}

// Declaration Merging
interface ShopItem2 {
    reviews: {
        rating: number,
        content: string
    }[]
}

declare global {
    interface Window {
        isDevelopment: boolean
    }
}

// Property Access Modifiers
class Article4 {
    public title: string
    private price: number
    constructor(title: string, price: number) {
        this.title = title
        this.price = price
    }
}
const article
    = new Article4('Smashing Book 6', 39)
//console.log(article.price) nejde

// Abstract Classes
abstract class Discount2 {
    // This needs to be implemented
    abstract isValid(article: Article): boolean;
    // This is already implemented
    apply(article: Article) { // Like before ...
    }
}

// Enums
enum UserType {
    Admin,
    PayingCustomer,
    Trial
}
function showWarning(user: UserType) {
    switch (user) {
        case UserType.Admin: return false;
        case UserType.PayingCustomer: return false;
        case UserType.Trial: return false;
    }
}

// funkce
// A helper type with the results we expect // from calling the back end
type Result = {
    title: string, url: string, abstract: string
}
/**
* The search function takes a query it sends * to the back end, as well as a couple of tags * as a string array, to get filtered results */
declare function search(query: string,
    tags?: string[]
): Result[]

search('Ember') // Yes!
search('Ember', ['JavaScript']) // Also yes! 
search('Ember', ['JavaScript', 'CSS']) // Yes yes!

// Asynchronous Back-End Calls
function search2(query: string, tags?: string[]): Promise<Result[]> { // Based on our input parameters, we build a query // string
    let queryString = `?query=${query}`
    // tags can be undefined as it's optional. // let's check if they exist
    if (tags && tags.length) {
        // and add all tags in that array to the // query string
        queryString += `&tags=${tags.join()}`
    }
    // Ready? Fetch from our search API
    return fetch(`/search${queryString}`)
        .then(response => response.json())
}

// Callbacks
type SearchFn = typeof search2


// Function Types in Objects
type Query = {
    query: string,
    tags?: string[],
    assemble: (includeTags: boolean) => string
}

const query: Query = {
    query: 'Ember',
    tags: ['javascript'], assemble(includeTags = false) {
        let query = `?query=${this.query}`
        if (includeTags && typeof this.tags !== 'undefined') {
            query += `&${this.tags.join(',')}`
        }
        return query
    }
}